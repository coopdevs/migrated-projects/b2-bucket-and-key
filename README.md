# Backblaze bucket and key

_Create a new private bucket and a limited key to this bucket._

## Goal

This script helps us when we are deploying new hosts with ansible and don't
want to click a zillion times on B2 website. This client is pretty
straightforward, only does what it says and lets you get your bucket name wrong
without having to input again your keys.

## Inputs

### B2 Keys

You need the _Master key_ or _Account ID_ from backblaze, as well as its
secret. The keys you can create from the UI don't have the `writeKeys`
capability, needed to create other keys.

> Note for Coopdevs: We keep this keys in Bitwarden.

### Bucket name

The desired name for your new b2 bucket. Surprisingly enough, it is a unique name inside all b2, so it's wise to prefix your buckets with a namespace to avoid the situation when you find out that a name you wanted to use is unavailable.

Example not to follow:
```
staging  → ok!
staging2 → ok!
production   → already taken! :(
```

Example to follow:
```
myorg-myapp-staging-1 → ok!
myorg-myapp-staging-2 → ok!
myorg-myapp-production-1 → ok!
```

Also, as all application keys have a label name, we use the bucket name for the key. This is only used as a human-friendly feature, as it can change and therefore doesn't identify a key.

## Outputs

* `bucket_name`: name and identifier of the newly created bucket.
* _`app_key_name`: label name of the newly created application key._
* `app_key_id`: the unique identifier of your new key.
* `app_key_value`: the secret, value or 'actual key', of the new 'application key'.

## Python requirements

* base64
* requests
* json

## Sample execution

```
Input your master key id: xxxxxxxxxxxx
Input your master key value: xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
Input your bucket and new app key name: my-bucket

New private bucket has been created
  bucket_name: my-bucket

New application key for bucket "my-bucket" has been created
  app_key_name: my-bucket
  app_key_id: xxxxxxxxxxxxxxxxxxxxxxxxx
  app_key_value: xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

Save well this data and reset the terminal to avoid leaking it

```
