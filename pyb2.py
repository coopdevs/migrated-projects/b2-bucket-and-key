#!/usr/bin/env python3
import base64
import json
import requests

def explain(response):
    print('{} ({}): {}'.
        format(response['status'], response['code'], response['message'] )
    )

def main():

    ########################
    ## Get keys from user ##

    key_id = ''
    key_value = ''

    while not key_id:
        key_id = input('Input your master key id: ')
    while not key_value:
        key_value = input('Input your master key value: ')

    ############################
    ## Authentication request ##
    # https://www.backblaze.com/b2/docs/b2_authorize_account.html

    id_and_key = key_id + ':' + key_value
    basic_auth_string = 'Basic ' + ( base64.b64encode(str.encode(id_and_key)) ).decode()
    headers = { 'Authorization': basic_auth_string }
    url = 'https://api.backblazeb2.com/b2api/v2/b2_authorize_account'
    r = requests.get(url, headers=headers)
    j = r.json()

    if r.status_code != 200:
        explain(j)
        return 1

    if "writeKeys" not in j['allowed']['capabilities'] or \
       "writeBuckets" not in j['allowed']['capabilities']:
        print('This key doesn\'t have enough capabilities. Are you sure this is a master key?')
        return 1

    token = j['authorizationToken']
    host = j['apiUrl']

    ###########################
    ## Create private bucket ##
    # https://www.backblaze.com/b2/docs/b2_create_bucket.html

    status = 0
    while status != 200:

        # Get bucket name
        bucket_name = ''
        while not bucket_name:
            bucket_name = input('Input your bucket and new app key name: ')

        # We want private bucket. URL provided by API
        bucket_type = "allPrivate" # Either allPublic or allPrivate
        url = host + '/b2api/v2/b2_create_bucket'

        # Prepare request
        params = {
            'accountId': key_id,
            'bucketName': bucket_name,
            'bucketType': bucket_type
        }
        headers = { 'Authorization': token }

        # Perform request
        r = requests.post(url, json=params, headers=headers)

        # Save data for next step or loop
        j = r.json()
        status = r.status_code

        # In case of error, an explanation is hold in body
        if status != 200: explain(j)

    # Success!
    print("\nNew private bucket has been created")
    print("  bucket_name: {}".format(bucket_name))

    ############################
    ## Create application key ##
    # https://www.backblaze.com/b2/docs/application_keys.html#capabilities

    # Prepare request
    caps = [
        "listBuckets", "shareFiles",
        "deleteFiles", "listFiles",
        "readFiles", "writeFiles"
    ]

    params = {
        'accountId': key_id,
        'capabilities': caps,
        'keyName': bucket_name,
        'bucketId': j['bucketId']
    }

    url = host + '/b2api/v2/b2_create_key'

    # Perform request
    r = requests.post(url, json=params, headers=headers)

    # Save data
    j = r.json()

    if r.status_code != 200:
        explain(j)
        return 1

    # Prompt the relevant information of the reply. Namely, credentials
    print("\nNew application key for bucket \"{}\" has been created".
            format(bucket_name))
    print("  app_key_name: {}\n  app_key_id: {}\n  app_key_value: {}".
            format(bucket_name, j['applicationKeyId'], j['applicationKey']))
    print("\nSave well this data and reset the terminal to avoid leaking it")


if __name__== "__main__":
    main()
